#!/bin/bash

# This file tests the latest and slim build and then compares
# their outputs to ensure they are the same.

cd ./test/example || exit 1
echo "Testing the regular image"
LATEST_OUTPUT=$(docker run -v ${PWD}:/work -w /work megabytelabs/mdspell:latest mdspell test.md)
echo $LATEST_OUTPUT

echo "Testing the slim image"
SLIM_OUTPUT=$(docker run -v ${PWD}:/work -w /work megabytelabs/mdspell:slim mdspell test.md)
echo $SLIM_OUTPUT

echo "Comparing the output from the regular image and slim image"

if [ "$LATEST_OUTPUT" == "$SLIM_OUTPUT" ]; then
    echo "The output from the slim image matches the output from the regular image"
    exit 0
else
    echo "ERROR: The output from the slim image does not match the output from the regular image"
    exit 1
fi
